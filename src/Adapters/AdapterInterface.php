<?php
namespace Framework\Cache\Adapters;

interface AdapterInterface
{

    public function has(string $key, int $ttl);

    public function get(string $key, $default = null);

    public function set(string $key, $value, int $ttl);

    public function delete(string $key);

    public function flush();
}
