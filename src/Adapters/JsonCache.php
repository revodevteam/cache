<?php
namespace Framework\Cache\Adapters;

use function Framework\Smarter\json_encode;
use function Framework\Smarter\json_decode;

class JsonCache extends NativeCache
{

    protected function cacheFileFromKey(string $key)
    {
        return $this->directory . DS . $key . '.json';
    }

    protected function pack($data)
    {
        return json_encode($data);
    }

    protected function unpack(string $payload)
    {
        return json_decode($payload);
    }
}
