<?php

namespace Framework\Cache\Adapters;

class MemCached implements AdapterInterface
{

    /** @var \Memcache MemCache Driver */
    private static $server;

    public function __construct(string $host = 'localhost', int $port = 11211)
    {
        $this->connect($host, $port);
    }

    public function connect(string $host, int $port): MemCached
    {
        if (empty(self::$server)) {
            self::$server = new \MemCached();
            self::$server->addserver($host, $port);
        }

        return $this;
    }

    public function get($key)
    {
        return self::$server->get($key);
    }

    public function set($key, $value, int $ttl = 0): MemCached
    {
        self::$server->set($key, $value, $ttl);

        return $this;
    }

    public function delete($key)
    {
        self::$server->delete($key);

        return $this;
    }

    public function flush()
    {
        return self::$server->flush();
    }

    public function has(string $key, int $ttl)
    {
        
    }
}
