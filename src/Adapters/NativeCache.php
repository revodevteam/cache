<?php
namespace Framework\Cache\Adapters;

class NativeCache implements AdapterInterface
{

    protected $directory;

    public function __construct($directory = ROOT_DIR . DS . 'var' . DS . 'caches')
    {
        $this->directory = $directory;
        $this->connect();
    }

    protected function connect()
    {
        $this->checkDIR($this->directory);
    }

    protected function checkDIR(string $directory)
    {
        if (is_dir($directory) === false) {
            mkdir($directory, 0755, true);
        }
    }

    public function has(string $key, int $ttl)
    {
        
    }

    public function get(string $key, $ttl = 3600)
    {
        $path = $this->cacheFileFromKey($key);

        if (file_exists($path)) {

            if ((filemtime($path) + $ttl) > time()) {
                return $this->read($path);
            }
        }

        return null;
    }

    public function set(string $key, $value, int $ttl = 0)
    {
        $path = $this->cacheFileFromKey($key);
        $this->checkDIR(dirname($path));

        $write = $this->write($path, $value);

        if ($write !== false) {
            return true;
        } else {
            throw new \Exception("Cache cant write !");
        }
    }

    public function delete(string $key)
    {
        $file = $this->cacheFileFromKey($key);

        if (file_exists($file)) {
            return unlink($file);
        }

        return false;
    }

    public function flush($subDirectory = false)
    {
        if ($subDirectory !== false) {
            $directory = $this->directory . DS . $subDirectory;
        } else {
            $directory = $this->directory;
        }

        if (is_dir($directory) === false) {
            return true;
        }

        $iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($directory, \RecursiveDirectoryIterator::SKIP_DOTS), \RecursiveIteratorIterator::SELF_FIRST);

        foreach ($iterator as $file) {
            if ($file->isFile()) {
                unlink($file->getPathname());
            }
        }

        return true;
    }

    protected function cacheFileFromKey(string $key)
    {
        return $this->directory . DS . $key . '.cache';
    }

    /**
     * 
     * @param string $file
     * @param mixed $payload
     * @return type
     */
    protected function write(string $file, $payload)
    {
        return file_put_contents($file, $this->pack($payload), LOCK_EX);
    }

    /**
     * 
     * @param string $file
     * @return mixed
     */
    protected function read(string $file)
    {
        return $this->unpack(file_get_contents($file));
    }

    /**
     * serialize data into string
     * @param mixed $data
     * @return string
     */
    protected function pack($data)
    {
        return serialize($data);
    }

    /**
     * deserialize string into php data
     * @param string $payload
     * @return mixed
     */
    protected function unpack(string $payload)
    {
        return unserialize($payload);
    }
}
