<?php
namespace Framework\Cache\Adapters;

class NoCache implements AdapterInterface
{

    public function __construct()
    {
        
    }

    public function has(string $key, int $ttl)
    {
        return false;
    }

    public function get(string $key, $ttl = 3600)
    {
        return null;
    }

    public function set(string $key, $value, int $ttl = 0)
    {
        return true;
    }

    public function delete(string $key)
    {
        return false;
    }

    public function flush($subDirectory = false)
    {
        return true;
    }
}
