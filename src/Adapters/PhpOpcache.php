<?php
namespace Framework\Cache\Adapters;

class PhpOpcache extends NativeCache
{

    protected function cacheFileFromKey(string $key)
    {
        return $this->directory . DS . $key . '.php';
    }

    /**
     * 
     * @param string $file
     * @return mixed
     */
    protected function read(string $file)
    {
        return (require $file);
    }

    protected function pack($data)
    {
        return '<?php return ' . var_export($data, true) . ';';
    }

    protected function unpack(string $payload)
    {
        
    }
}
