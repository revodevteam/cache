<?php

namespace Framework\Cache\Adapters;

use Predis;

class Redis implements AdapterInterface
{

    /** @var Predis\Client MemCache Driver */
    private static $server;

    public function __construct(string $host = '127.0.0.1', int $port = 6379, $scheme = 'tcp')
    {
        $this->connect($host, $port, $scheme);
    }

    public function connect(string $host, int $port, string $scheme): Redis
    {
        \Vendor\Predis\src\Autoloader::register();

        if (empty(self::$server)) {
            self::$server = new Predis\Client();
        }

        return $this;
    }

    public function get($key)
    {
        return self::$server->get($key);
    }

    public function set($key, $value, int $ttl = 0): Redis
    {
        self::$server->set($key, $value);
        self::$server->expire($key, $ttl);

        return $this;
    }

    public function delete($key)
    {
        self::$server->delete($key);

        return $this;
    }

    public function flush(): Redis
    {
        self::$server->flush();

        return $this;
    }

    public function has(string $key, int $ttl)
    {
        
    }
}
