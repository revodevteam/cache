<?php
namespace Framework\Cache;

use Framework\Cache\Adapters\Redis;
use Framework\Cache\Adapters\NoCache;
use Framework\Cache\Adapters\JsonCache;
use Framework\Cache\Adapters\MemCached;
use Framework\Cache\Adapters\PhpOpcache;
use Framework\Cache\Adapters\NativeCache;

class CacheFactory
{

    public static function build(string $client = CACHE_CLIENT, ...$arguments)
    {
        switch ($client) {
            case 'Native':
                $cachier = new NativeCache(...$arguments);
                break;
            case 'PHP':
                $cachier = new PhpOpcache(...$arguments);
                break;
            case 'Json':
                $cachier = new JsonCache(...$arguments);
                break;
            case 'MemCached':
                $cachier = new MemCached(...$arguments);
                break;
            case 'Redis':
                $cachier = new Redis(...$arguments);
                break;
            case 'NoCache':
                $cachier = new NoCache(...$arguments);
                break;

            default:
                $cachier = new NativeCache(...$arguments);
                break;
        }

        return $cachier;
    }
}
